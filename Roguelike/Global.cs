﻿using RogueSharp.Random;

namespace Roguelike
{

    // ok, so this enum is apparently a bad way to do a finite state machine.
    // causes problems as the game scales.
    // but it's ok as an example
    // learn about finite state machines later and apply this knowledge here
    public enum GameStates
    {
        None = 0,
        PlayerTurn = 1,
        EnemyTurn = 2,
        Debugging = 3
    }

    public class Global
    {
        public static CombatManager CombatManager;

        public static readonly IRandom Random = new DotNetRandom();
        public static GameStates GameState { get; set; }

        public static readonly int MapWidth = 50;
        public static readonly int MapHeight = 30;
        public static readonly int SpriteWidth = 24;
        public static readonly int SpriteHeight = 24;

        public static readonly Camera Camera = new Camera();
    }
}
