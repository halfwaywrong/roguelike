﻿
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RogueSharp.DiceNotation;

namespace Roguelike
{
    public class Figure
    {
        public int X { get; set; }
        public int Y { get; set; }
        public AnimatedSprite Sprite { get; set; }
        public float Scale { get; set; }

        // Roll a 20-sided die and add this value when making an attack
        public int AttackBonus { get; set; }
        // An attack must meet or exceed this value to hit
        public int ArmorClass { get; set; }
        // Roll these dice to determine how much damage was dealt after a hit
        public DiceExpression Damage { get; set; }
        // How many points of damage the figure can withstand before dieing
        public int Health { get; set; }
        // The name of the figure, used for attack messages
        public string Name { get; set; }

        public void UpdateAnimation(GameTime gameTime)
        {
            Sprite.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            float multiplier = Scale * 24;
            Sprite.Draw(spriteBatch, new Vector2(X * multiplier, Y * multiplier));
        }
    }
}
